const Selector = document.querySelectorAll.bind(document);
const SelectorById = document.querySelector.bind(document);
let edit = Selector("#edit");
let rate = Selector("#rate");
let del = Selector("#delete");
let rateModal = SelectorById("#rateModal");
let filter = SelectorById("#filter");
let btnExport = SelectorById("#btnExport");

rate.forEach((btn) => {

  btn.addEventListener("click", (event) => {

    $("#myModalRate").modal("show");

    rateModal.addEventListener("click", async (e) => {

      let stars = SelectorById("#stars");

      let req = await fetch(`http://${window.location.hostname}:3000/rate`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: `valueOfRate=${stars.value}&feedbackId=${btn.dataset.id}`
      });

      if(req){
        location.reload();
      }
    });
  });
});

del.forEach((btn) => {

  btn.addEventListener("click", (event) => {
    $("#myModalDelete").modal("show");

    deleteFeedback.addEventListener("click", async (e) => {

      let req = await fetch(`http://${window.location.hostname}:3000/delete`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: `feedbackId=${btn.dataset.id}`
      });

      if(req){
        location.reload();
      }
    });
  });
});

edit.forEach((btn) => {
  btn.addEventListener("click", (event) => {

    window.location.href = `edit/${btn.dataset.id}`;
  });
});

filter.addEventListener("keyup", function(){
  $("table > tbody >  tr").filter(function() {
    $(this).toggle($(this).text().indexOf(filter.value) > -1)
  });
});

btnExport.addEventListener("click", (event) => {

  let uri = 'data:application/vnd.ms-excel;base64,',
  template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
  base64 = function(s) {
    return window.btoa(unescape(encodeURIComponent(s)))
  },
  format = function(s, c) {
    return s.replace(/{(\w+)}/g, function(m, p) {
      return c[p];
    })
  }
  let toExcel = document.getElementById("table").innerHTML;
  let ctx = {
    worksheet: name || '',
    table: toExcel
  };
  let link = document.createElement("a");
  link.download = "sthima-manager.xls";
  link.href = uri + base64(format(template, ctx))
  link.click();
});
