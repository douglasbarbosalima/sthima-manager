const ObjectID = require('mongodb').ObjectID;

class ManagerModel {

	constructor(connection) {

		this._connection = connection();
	}

	login(email, password) {

		return new Promise((resolve, reject) => {
			this._connection.open((err, mongoClient) => {
				if(err) reject(err);
				mongoClient.collection('manager', (error, collection) => {
					if(err) reject(error);
					collection.find({'email': { $eq: email}, 'password': { $eq: password }}).toArray((errorFind, docCollection) => {
						if(errorFind) reject(errorFind);

						resolve(docCollection.length);
						mongoClient.close();
					});

				});
			});
		});
	}

	feedback(nameEmployee, emailEmployee, departmentEmployee, managerFeedback, feedbackEmployee) {

		return new Promise((resolve, reject) => {
			this._connection.open((err, mongoClient) => {
				if(err) reject(err);
				mongoClient.collection('feedback', (error, collection) => {
					if(err) reject(error);
					collection.insert({
						nameEmployee:  nameEmployee,
						emailEmployee: emailEmployee,
						departmentEmployee: departmentEmployee,
						managerFeedback: managerFeedback,
						feedbackEmployee: feedbackEmployee,
						date: new Date(),
						rate: 0
					});

					resolve("Well Done!");

					mongoClient.close();
				})
			});
		});
	}

	register(username, email, password) {

		return new Promise((resolve, reject) => {
			this._connection.open((err, mongoClient) => {
				if(err) reject(err);
				mongoClient.collection('manager', (error, collection) => {
					if(error) reject(error);
					collection.insert({
						username: username,
						email: email,
						password: password
					});

					resolve("Well Done!");

					mongoClient.close();
				});
			});
		});
	}

	getFeedbackByManager(manager){
		return new Promise((resolve, reject) => {
			this._connection.open((err, mongoClient) => {
				if(err) reject(err);
				mongoClient.collection('feedback', (error, collection) => {
					if(err) reject(error);
					collection.find({'managerFeedback': { $eq: manager}}).toArray((errorFind, docCollection) => {
						if(errorFind) reject(errorFind);

						resolve(docCollection);
						mongoClient.close();
					});

				});
			});
		});
	}

	getManagers(){
		return new Promise((resolve, reject) => {
			this._connection.open((err, mongoClient) => {
				if(err) reject(err);
				mongoClient.collection('manager', (error, collection) => {
					if(err) reject(error);
					collection.find().toArray((errorFind, docCollection) => {
						if(errorFind) reject(errorFind);

						resolve(docCollection);
						mongoClient.close();
					});

				});
			});
		});
	}

	getManagerById(manager){
		return new Promise((resolve, reject) => {
			this._connection.open((err, mongoClient) => {
				if(err) reject(err);
				mongoClient.collection('manager', (error, collection) => {
					if(err) reject(error);
					collection.find({ 'email': {$eq: manager} }).toArray((errorFind, docCollection) => {
						if(errorFind) reject(errorFind);

						resolve(docCollection);
						mongoClient.close();
					});

				});
			});
		});
	}

	rateFeedback(stars, id){
		return new Promise((resolve, reject) => {
			this._connection.open((err, mongoClient) => {
				if(err) reject(err);
				mongoClient.collection('feedback', (error, collection) => {
					if(err) reject(error);
					collection.updateOne({_id: {$eq: new ObjectID(id)} }, {$set: {'rate': stars}}, function(err, obj){
						if(err) reject(err);
						resolve(true);


						mongoClient.close();
					});

				});
			});
		});
	}

	deleteFeedback(id){
		return new Promise((resolve, reject) => {
			this._connection.open((err, mongoClient) => {
				if(err) reject(err);
				mongoClient.collection('feedback', (error, collection) => {
					if(err) reject(error);
					collection.remove({_id: {$eq: new ObjectID(id)} }, function(err, obj){
						if(err) reject(err);
						resolve(true);


						mongoClient.close();
					});

				});
			});
		});
	}
}

module.exports = function(){
	return ManagerModel;
}