const ManagerModel  = require('./../models/Manager')();
const Helper = require('./../helpers/Helper');

module.exports.register = async (application, req, res) => {

	let { username, email, password, passwordConfirm } = req.body;
	const confirm = Helper.validateRegister(password, passwordConfirm);

	if(confirm){

		let connection = application.config.dbConnection;
		let Model = new ManagerModel(connection);
		const verify = await Model.register(username, email, password);

		if(verify) {
			res.redirect('./../../login');
			return;
		}
		res.redirect('./../../login');
	}

	res.redirect('./../../register');
	return;
}

module.exports.login = async (application, req, res) => {

	let { email, password } = req.body;
	let connection = application.config.dbConnection;
	let Model = new ManagerModel(connection);
	const verify = await Model.login(email, password);

	if(verify){
		req.session.auth = true;
		req.session.email = email;
		res.redirect('./../../manager');
		return;
	}

	res.redirect('./../login');
	return;

}

module.exports.feedback = async (application, req, res) => {

	let { name, email, department, manager, feedback} = req.body;
	let connection = application.config.dbConnection;
	let Model = new ManagerModel(connection);
	const verify = await Model.feedback(name, email, department, manager, feedback);

	if(verify){

		res.redirect('./../');
		return;
	}

	res.redirect('./../', { errors: verify });
}

module.exports.checkout = (application, req, res) => {

	req.session.auth = false;
	req.session.email = null;
	res.redirect('./../login');
}

module.exports.manager = async (application, req, res) => {

	if(req.session.auth) {

		let connection = application.config.dbConnection;
		let Model = new ManagerModel(connection);

		const feedbacks = await Model.getFeedbackByManager(req.session.email);
		const managerInfo = await Model.getManagerById(req.session.email);

		res.render('manager', { feedback: feedbacks, manager: managerInfo });
		return;
	}

	res.redirect('./../login');
	return;
}

module.exports.handleFeedback = async (application, req, res) => {

	let connection = application.config.dbConnection;
	let Model = new ManagerModel(connection);

	const managers = await Model.getManagers();

	res.render('feedback', { managers: managers });
}

module.exports.rate = async (application, req, res) => {

	let { valueOfRate, feedbackId } = req.body;
	let connection = application.config.dbConnection;
	let Model = new ManagerModel(connection);

	feedBackId = await Helper.transformObjectID(feedbackId);
	const rated = await Model.rateFeedback(valueOfRate, feedbackId);


	res.redirect('./../manager');
}

module.exports.delete = async (application, req, res) => {

	let { feedbackId } = req.body;
	let connection = application.config.dbConnection;
	let Model = new ManagerModel(connection);

	feedBackId = await Helper.transformObjectID(feedbackId);
	const rated = await Model.deleteFeedback(feedbackId);

	res.redirect('./../manager');
}
