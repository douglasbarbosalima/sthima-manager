const ManagerModel  = require('./../models/Manager')();
const Helper = require('./../helpers/Helper');

module.exports.registerTest = async (application, req, res) => {

	let { username, email, password, passwordConfirm } = req.body;
	const confirm = Helper.validateRegister(password, passwordConfirm);

	if(confirm){

		let connection = application.config.dbConnection;
		let Model = new ManagerModel(connection);
		const verify = await Model.register(username, email, password);

		if(verify) {
			res.send({
				status: 200,
				message: 'Well Done!'
			});
			return;
		}
	}
	res.send({
		status: 500,
		message: 'Something is wrong :('
	});
	return;
}

module.exports.loginTest = async (application, req, res) => {

	let { email, password } = req.body;
	let connection = application.config.dbConnection;
	let Model = new ManagerModel(connection);
	const verify = await Model.login(email, password);

	if(verify){
		req.session.auth = true;
		req.session.email = email;
		res.send({
			status: 200,
			message: 'Well Done!'
		});
		return;
	}
	res.send({
		status: 500,
		message: 'Something is wrong :('
	});
	return;

}

module.exports.feedbackTest = async (application, req, res) => {

	let { name, email, department, manager, feedback} = req.body;
	let connection = application.config.dbConnection;
	let Model = new ManagerModel(connection);
	const verify = await Model.feedback(name, email, department, manager, feedback);

	if(verify){
		res.send({
			status: 200,
			message: 'Well Done!'
		});
		return;
	}

	res.send({
		status: 500,
		message: 'Something is wrong :('
	});
	return;
}

module.exports.checkoutTest = (application, req, res) => {

	req.session.auth = false;
	req.session.email = null;

	res.send({
		status: 200,
		message: 'Well Done!',
		session: req.session.auth
	});
}

module.exports.managerTest = async (application, req, res) => {

	if(req.session.auth) {

		let connection = application.config.dbConnection;
		let Model = new ManagerModel(connection);
		
		const feedbacks = await Model.getFeedbackByManager(req.session.email);
		const managerInfo = await Model.getManagerById(req.session.email);

		res.send({
			status: 200,
			message: 'Well Done!'
		});
		return;
	}

	res.status(500);
	res.send({
		status: 500,
		message: 'Something is wrong :('
	});
	return;
}

module.exports.handleFeedbackTest = async (application, req, res) => {

	let connection = application.config.dbConnection;
	let Model = new ManagerModel(connection);

	const managers = await Model.getManagers();

	if(managers) {
		res.send({
			status: 200,
			message: 'Well Done!'
		});
		return;
	}

	res.send({
		status: 500,
		message: 'Something is wrong :('
	});
	return;

}

module.exports.rateTest = async (application, req, res) => {

	let { valueOfRate, feedbackId } = req.body;
	let connection = application.config.dbConnection;
	let Model = new ManagerModel(connection);

	feedBackId = await Helper.transformObjectID(feedbackId);
	const rated = await Model.rateFeedback(valueOfRate, feedbackId);

	if(rated) {
		res.send({
			status: 200,
			message: 'Well Done!'
		});
		return;
	}

	res.send({
		status: 500,
		message: 'Something is wrong :('
	});
	return;

}

module.exports.deleteTest = async (application, req, res) => {

	let { feedbackId } = req.body;
	let connection = application.config.dbConnection;
	let Model = new ManagerModel(connection);

	feedBackId = await Helper.transformObjectID(feedbackId);
	const deleted = await Model.deleteFeedback(feedbackId);

	if(deleted) {
		res.send({
			status: 200,
			message: 'Well Done!'
		});
		return;
	}

	res.send({
		status: 500,
		message: 'Something is wrong :('
	});
	return;

}

module.exports.test = (application, req, res) => {
	res.send({
		status: 200,
		message: 'Well Done!'
	});
}
