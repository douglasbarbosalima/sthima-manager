const Manager = require('./../controllers/Manager');

module.exports = application => {

	application.get('/', (req, res) => {
		application.app.controllers.Manager.handleFeedback(application, req, res);
	});
	application.get('/login', (req, res) => {
		res.render('login');
	});
	application.get('/register', (req, res) => {
		res.render('register');
	});
	application.post('/create/account', (req, res) => {
		application.app.controllers.Manager.register(application, req, res);
	});
	application.post('/login/account', (req, res) => {
		application.app.controllers.Manager.login(application, req, res);
	});
	application.post('/feedback', (req, res) => {
		application.app.controllers.Manager.feedback(application, req, res);
	});
	application.get('/manager', (req, res) => {
		application.app.controllers.Manager.manager(application, req, res);
	});
	application.get('/checkout', (req, res) => {
		application.app.controllers.Manager.checkout(application, req, res);
	});
	application.post('/rate', (req, res) => {
		application.app.controllers.Manager.rate(application, req, res);
	});
	application.post('/delete', (req, res) => {
		application.app.controllers.Manager.delete(application, req, res);
	});
	application.get('/test', (req, res) => {
		application.app.controllers.Manager.checkout(application, req, res);
	});
	application.use((req, res) => {
		return res.status(404).render('404');
	});
}
