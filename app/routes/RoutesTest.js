const Manager = require('./../controllers/Manager');

module.exports = application => {

	application.post('/create/account/test', (req, res) => {
		application.app.controllers.ManagerTest.registerTest(application, req, res);
	});
	application.post('/login/account/test', (req, res) => {
		application.app.controllers.ManagerTest.loginTest(application, req, res);
	});
	application.post('/feedback/test', (req, res) => {
		application.app.controllers.ManagerTest.feedbackTest(application, req, res);
	});
	application.get('/manager/test', (req, res) => {
		application.app.controllers.ManagerTest.managerTest(application, req, res);
	});
	application.get('/checkout/test', (req, res) => {
		application.app.controllers.ManagerTest.checkoutTest(application, req, res);
	});
	application.post('/rate/test', (req, res) => {
		application.app.controllers.ManagerTest.rateTest(application, req, res);
	});
	application.post('/delete/test', (req, res) => {
		application.app.controllers.ManagerTest.deleteTest(application, req, res);
	});
	application.get('/test/test', (req, res) => {
		application.app.controllers.ManagerTest.test(application, req, res);
	});
}