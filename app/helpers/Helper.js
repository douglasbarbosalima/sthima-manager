const ObjectID = require('mongodb').ObjectID;

class Helper {

	static validateRegister(password, confirmPassword){
		return password === confirmPassword ? true : false;
	}

	static transformObjectID(_id){

		return new ObjectID(_id);
	}
}

module.exports = Helper;
