//Import the config's from server.
const app = require('./config/server');
const moment = require('moment')();
const port = 3000;

/* Paramter the listen Port*/
app.listen(port, function(){
	console.log('Server is on: ' + moment.format());
});