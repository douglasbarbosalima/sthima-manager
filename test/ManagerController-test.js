const chai = require('chai');
const chaiHttp = require('chai-http');
const assert = chai.assert;
const expect = chai.expect;
const should = chai.should();
const application = require('./../config/server.js');

chai.use(chaiHttp);

describe('Test Controller Manager', () => {
	it('Generic Test', (done) => {
		chai.request(application)
		.get('/test/test')
		.end((err, res) => {
			expect(res).to.have.status(200);
			expect(res.body.message).to.equal('Well Done!');
			done();
		});
	});

	it('Register on Platform', (done) => {
		chai.request(application)
		.post('/create/account/test')
		.type('form')
		.send({
			'username': 'User Test', 
			'email': 'test@sthima.com',
			'password': 'abcOIAHdiB===',
			'passwordConfirm': 'abcOIAHdiB==='
		})
		.end((err, res) => {
			expect(res).to.have.status(200);
			expect(res.body.message).to.equal('Well Done!');
			done();
		});
	});

	it('Login on Platform', (done) => {
		chai.request(application)
		.post('/login/account/test')
		.type('form')
		.send({
			'email': 'douglasbarbosadelimafald@gmail.com',
			'password': '72249313'
		})
		.end((err, res) => {
			expect(res).to.have.status(200);
			expect(res.body.message).to.equal('Well Done!');
			done();
		});
	});

	it('Register a new Feedback', (done) => {
		chai.request(application)
		.post('/feedback/test')
		.type('form')
		.send({
			'username': 'User Test', 
			'email': 'test@sthima.com',
			'password': 'abcOIAHdiB===',
			'passwordConfirm': 'abcOIAHdiB==='
		})
		.end((err, res) => {
			expect(res).to.have.status(200);
			expect(res.body.message).to.equal('Well Done!');
			done();
		});
	});

	it('Get Data from Manager', (done) => {
		chai.request(application)
		.get('/manager/test')
		.end((err, res) => {
			expect(res).to.have.status(500);
			expect(res.body.message).to.equal('Something is wrong :('); // In this case we testing a error because of Authentication.
			done();
		});
	});

	it('Logout Platform', (done) => {
		chai.request(application)
		.get('/checkout/test')
		.end((err, res) => {
			expect(res).to.have.status(200);
			expect(res.body.message).to.equal('Well Done!');
			done();
		});
	});

	it('Rate Feedback', (done) => {
		chai.request(application)
		.post('/rate/test')
		.type('form')
		.send({
			'valueOfRate': 5, 
			'feedbackId': '5c150252ec77d0270a2643e5'
		})
		.end((err, res) => {
			expect(res).to.have.status(200);
			expect(res.body.message).to.equal('Well Done!');
			done();
		});
	});

	it('Delete Feedback', (done) => {
		chai.request(application)
		.post('/delete/test')
		.type('form')
		.send({
			'feedbackId': '5c150252ec77d0270a2643e5'
		})
		.end((err, res) => {
			expect(res).to.have.status(200);
			expect(res.body.message).to.equal('Well Done!');
			done();
		});
	});
})

