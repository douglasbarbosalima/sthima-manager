const mongo = require('mongodb');

const connMongoDB = () => {
	let db = new mongo.Db(
		'shitma',
		new mongo.Server(
			'localhost',
			27017,
			{}
		),
		{}
	);

	return db;
}

module.exports = function(){
	return connMongoDB;
}
