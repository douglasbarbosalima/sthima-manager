# Sthima - Manager

- This is a generic Platform for Manager's read the feedback's from your squad.

#note
	- You need of MongoDB and NodeJS installed and configured on your machine, after of run this project.
	- Clone this project with HTTPS.

#Rules of Business Note:
	- Before going to '/' route. You need a Register a new Manager, you can access this feature on edpoint 3 below on Pages section.
For install Dependencies and Run Application

```npm install && node app.js
  ```

#Pages
1. '/' => Will redirect you for page where Employees Write the feedback for Managers.
2. '/login' => Will redirect you for page where Manager's Log in on Platform.
3. '/register' => Will redirect you for page where Manager's register on Platform.

#Test
	- For run Test's on ManagerController, do you need run this commmand on default Path of Project:

```mocha test/ManagerController-test.js
	```